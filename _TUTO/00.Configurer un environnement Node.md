# Configurer un environnement Node

## Installer Node

### Windows et macOS

Installer Node et NPM sur Windows and macOS est facile :

1. Télécharger l'installateur :
   - Aller sur https://nodejs.org/fr/
   - Sélectionner le bouton pour télécharger la version **LTS** qui est "Recommandé pour la plupart des utilisateurs".
   - Installer Node en double cliquant et suivre les informations.

### Ubuntu / Debian

Le plus simple pour installer la dernière LTS de Node est d'utiliser le gestionnaire de paquets.
On utilise cepdenant les sources venant de Node et non Ubuntu (très vieille version !).
Dans le terminal :

```bash
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
```

### Fedora / CentOS

Le plus simple pour installer la dernière LTS de Node est d'utiliser le gestionnaire de paquets.
On utilise cepdenant les sources venant de Node et non Ubuntu (très vieille version !).
Dans le terminal :

```bash
curl -sL https://rpm.nodesource.com/setup_10.x | bash -
```

## Tester l'installation

Dans le terminal :
```bash
$ node -v
v10.15.0
```

## Installer l'outil *Express Application Generator*

Avec le gestionnaire de paquet Node NPM (le drapeau `-g` installe l'outil de façon globale donc vous pouvez l'utiliser n'importe où) :
```bash
npm install -g express-generator
```
